import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function listAuthRole(query) {
  return request({
    url: api_prefix + 'auth_role/list',
    method: 'get',
    params: { ...query }
  })
}

export function getAllAuthRole() {
  return request({
    url: api_prefix + 'auth_role/all',
    method: 'get'
  })
}

export function getAuthRoleByID(id) {
  return request({
    url: api_prefix + 'auth_role/get_id',
    method: 'get',
    params: { id: id }
  })
}

export function addAuthRole(role) {
  return request({
    url: api_prefix + 'auth_role/add',
    method: 'post',
    data: { ...role }
  })
}

export function editAuthRole(role) {
  return request({
    url: api_prefix + 'auth_role/edit',
    method: 'put',
    data: { ...role }
  })
}

export function updateAuthRole(data) {
  return request({
    url: api_prefix + 'auth_role/update',
    method: 'put',
    data: { ...data }
  })
}

export function deleteAuthRole(ids) {
  return request({
    url: api_prefix + 'auth_role/delete',
    method: 'delete',
    data: { ids }
  })
}
