<?php


namespace app\common\exception;


class BannerException extends BaseException
{
    public $code = 404;
    public $msg = 'Banner不存在';
    public $errorCode = 40001;
}