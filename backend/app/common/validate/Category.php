<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-26
 * Time: 16:06
 */

namespace app\common\validate;


class Category extends BaseValidate
{
    public function __construct($data = [])
    {
        parent::__construct();

        $this->rule = [
            'id' => 'require|number|gt:0',
            'pid' => 'require|number',
            'title' => 'require|max:60|unique:Category,pid='.$data['pid'],
            'sort' => 'require|number|gt:0',
            'status' => 'require|number|between:0,1',
            'top_id' => 'require|number|gt:0',
        ];

        $this->message = [
            'id.require' => 'id不能为空',
            'id.number' => 'id必须为整数',
            'id.gt' => 'id必须大于0',
            'pid.require' => '上级栏目不能为空',
            'pid.number' => '上级栏目的值格式错误',
            'title.require' => '栏目名称不能为空',
            'title.max' => '栏目名称最大长度为60',
            'title.unique' => '栏目名称不能重复',
            'sort.require' => '排序值不能为空',
            'sort.number' => '排序值必须为数字',
            'sort.gt' => '排序的值必须大于0',
            'status.require' => '状态不能为空',
            'status.number' => '状态值的格式不正确',
            'status.between' => '状态值的格式不正确',
            'top_id.require' => '需要获取导航的顶级id不能为空',
            'top_id.number' => '需要获取导航的顶级id值格式错误',
            'top_id.gt' => '需要获取导航的顶级id值格式错误',
        ];

        $this->scene = [
            'add' => ['pid','title','sort','status'],
            'edit' => ['id','pid','title','sort','status'],
            'get_nav' => ['top_id'],
        ];
    }
}