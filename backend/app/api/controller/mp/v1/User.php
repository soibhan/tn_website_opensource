<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-02
 * Time: 19:17
 */

namespace app\api\controller\mp\v1;


use app\api\BaseController;
use app\common\model\WeChatUser as WeChatUserModel;
use app\common\model\MpApiUserToken;

class User extends BaseController
{
    /**
     * 更新用户的相关信息
     * @http PUT
     * @url /user/update
     * :nick_name 用户名
     * :avatar_url 用户头像信息
     * :gender 用户性别
     * @return \think\response\Json
     */
    public function updateUserInfo()
    {
        $data = $this->request->put(['nick_name','avatar_url','gender']);

        // 获取用户的uid
        $data['id'] = MpApiUserToken::getCurrentUID();
        // 设置用户来源(小程序)
        $data['from'] = 1;

        $result = WeChatUserModel::updateUserInfo($data);

        if ($result) {
            return tn_yes('更新用户信息成功');
        } else {
            return tn_no('更新用户信息失败');
        }
    }
}