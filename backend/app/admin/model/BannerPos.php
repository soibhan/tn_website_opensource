<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-25
 * Time: 13:51
 */

namespace app\admin\model;


use app\common\exception\ParameterException;
use app\common\model\Banner;
use app\common\model\BaseModel;
use app\common\validate\IDCollection;
use think\model\concern\SoftDelete;
use app\admin\validate\BannerPos as Validate;

class BannerPos extends BaseModel
{
    protected $hidden = ['update_time','delete_time'];

    // 使用软删除
    use SoftDelete;
    protected $deleteTime = 'delete_time';

    /**
     * 获取轮播位的分页数据
     * @param array $params
     * @return \think\Paginator
     */
    public static function getPaginationList(array $params)
    {
        static::validatePaginationData($params);

        $static = new static();

        foreach ($params as $name => $value) {
            $value = trim($value);
            switch ($name) {
                case 'title':
                    if (!empty($value)) {
                        $like_text = '%' . $value . '%';
                        $static = $static->whereLike('title', $like_text);
                    }
                    break;
            }
        }

        return $static
            ->paginate([
                'page' => $params['page'],
                'list_rows' => $params['limit']
            ], false);
    }

    /**
     * 获取全部轮播位的名称
     * @return array|\think\Collection
     */
    public static function getAllBannerPosTitle()
    {
        $data = static::field('id,title')
            ->select();

        $data = !empty($data) ? $data->toArray(): [];

        return $data;
    }

    /**
     * 添加轮播位信息
     * @param array $data
     * @return bool
     */
    public static function addBannerPos(array $data)
    {
        $validate = new Validate();
        if (!$validate->scene('add')->check($data)) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        $static = static::create($data);

        if (isset($static->id) && $static->id > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 编辑轮播位信息
     * @param array $data
     * @return bool
     */
    public static function editBannerPos(array $data)
    {
        $validate = new Validate();
        if (!$validate->scene('edit')->check($data)) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        $static = static::find($data['id']);
        $result = $static->allowField(['id','title'])
            ->save($data);

        if ($result !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 删除指定id的轮播位信息
     * @param $ids
     * @return bool
     */
    public static function deleteBannerPos($ids)
    {
        $validate = new IDCollection();
        if (!$validate->check(['ids' => $ids])) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }

        $atlas_data = Banner::where('pos_id','in', $ids)
            ->count('id');

        if (!empty($atlas_data)) {
            throw new HaveUseException([
                'msg' => '当前轮播位下还有轮播图',
                'errorCode' => 10008
            ]);
        }

        if (static::destroy($ids) !== false) {
            return true;
        } else {
            return false;
        }
    }
}