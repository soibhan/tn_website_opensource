<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-03
 * Time: 11:01
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class AuthMenu extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'pid' => 'require|number',
        'name' => 'require|max:60',
        'title' => 'require|max:60',
        'path' => 'require|max:255|checkPathIsUrl',
        'url' => 'max:255',
        'redirect' => 'max:255',
        'icon' => 'max:80',
        'is_link' => 'require|number|between:0,1',
        'hidden' => 'require|number|between:0,1',
        'is_click_in_breadcrumb' => 'require|number|between:0,1',
        'always_show' => 'require|number|between:0,1',
        'no_cache' => 'require|number|between:0,1',
        'breadcrumb_show' => 'require|number|between:0,1',
        'public_menu' => 'require|number|between:0,1',
        'sort' => 'require|number|gt:0',
        'status' => 'require|number|between:0,1',
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'pid.require' => 'pid不能为空',
        'pid.number' => 'pid必须为整数',
        'name.require' => '路由名称不能为空',
        'name.max' => '路由名称最大不能超过60',
        'title.require' => '菜单名称不能为空',
        'title.max' => '菜单名称最大不能超过60',
        'path.require' => '路由地址不能为空',
        'path.max' => '路由地址最大不能超过255',
        'path.checkPathIsUrl' => '填写的链接格式不正确',
        'url.max' => '文件所在位置最大255',
        'redirect.max' => '重定向地址最大255',
        'icon.max' => '图标名称最大80',
        'is_link.require' => '链接不能为空',
        'is_link.number' => '链接格式不正确',
        'is_link.between' => '链接格式不正确',
        'hidden.require' => '隐藏不能为空',
        'hidden.number' => '隐藏格式不正确',
        'hidden.between' => '隐藏格式不正确',
        'is_click_in_breadcrumb.require' => '面包屑中点击不能为空',
        'is_click_in_breadcrumb.number' => '面包屑中点击格式不正确',
        'is_click_in_breadcrumb.between' => '面包屑中点击格式不正确',
        'always_show.require' => '侧边栏总是显示不能为空',
        'always_show.number' => '侧边栏总是显示格式不正确',
        'always_show.between' => '侧边栏总是显示格式不正确',
        'no_cache.require' => '不缓存不能为空',
        'no_cache.number' => '不缓存格式不正确',
        'no_cache.between' => '不缓存格式不正确',
        'breadcrumb_show.require' => '面包屑中显示不能为空',
        'breadcrumb_show.number' => '面包屑中显示格式不正确',
        'breadcrumb_show.between' => '面包屑中显示格式不正确',
        'public_menu.require' => '公共菜单不能为空',
        'public_menu.number' => '公共菜单格式不正确',
        'public_menu.between' => '公共菜单格式不正确',
        'sort.require' => '排序不能为空',
        'sort.number' => '排序格式不正确',
        'sort.gt' => '排序格式不正确',
        'status.require' => '状态不能为空',
        'status.number' => '状态格式不正确',
        'status.between' => '状态格式不正确',
    ];

    protected $scene = [
        'add' => ['pid','name','title','path','url','redirect','icon','is_link','hidden','is_click_in_breadcrumb','always_show','no_cache','breadcrumb_show','sort','status'],
        'edit' => ['id','pid','name','title','path','url','redirect','icon','is_link','hidden','is_click_in_breadcrumb','always_show','no_cache','breadcrumb_show','sort','status'],
    ];

    /**
     * 检查path参数对应是否为链接时的规则是否正确
     * @param $value
     * @param string $rule
     * @param array $data
     * @param string $field
     * @return bool
     */
    protected function checkPathIsUrl($value,$rule = ''
        ,$data = [],$field = '')
    {
        if ($data['is_link'] == 1) {
            if (!(is_scalar($value) && 1 === preg_match('/^(ht|f)tps?:\/\/[\w-]+(\.[\w-]+)+(:\d{1,5})?\/$/', (string) $value))) {
                return false;
            }
        }
        return true;
    }
}