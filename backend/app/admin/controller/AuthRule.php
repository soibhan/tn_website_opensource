<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-05
 * Time: 09:30
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\admin\model\AuthRule as AuthRuleModel;

class AuthRule extends BaseController
{
    /**
     * 获取表格树形数据
     * @http get
     * @url /auth_rule/table_tree
     * @return \think\response\Json
     */
    public function getTableTree()
    {
        $params = $this->request->get();

        $data = AuthRuleModel::getTableTreeData($params);

        return tn_yes('获取TableTree数据成功',['data' => $data]);
    }

    /**
     * 获取树形结构数据
     * @http get
     * @url /auth_rule/element_tree
     * @return \think\response\Json
     */
    public function getElementTree()
    {
        $data = AuthRuleModel::getElementTreeData();

        return tn_yes('获取elementTree数据成功',['data' => $data]);
    }


    /**
     * 获取角色权限规则的所有节点信息
     * @http get
     * @url /auth_rule/all_rule
     * @return \think\response\Json
     */
    public function getAllRuleNode()
    {
        $data = AuthRuleModel::getAllRuleNode();

        return tn_yes('获取所有节点信息成功',['data' => $data]);
    }

    /**
     * 获取指定父节点下的子节点数量
     * @http get
     * @url /auth_rule/get_children_count
     * @return \think\response\Json
     */
    public function getChildrenCount()
    {
        $pid = $this->request->get('pid',0);

        $count = AuthRuleModel::getChildrenCount($pid);

        return tn_yes('获取当前父节点的子节点数量成功',['count' => $count]);
    }

    /**
     * 根据id获取角色权限规则的信息
     * @http get
     * @url /auth_rule/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id');

        $data = AuthRuleModel::getDataWithID($id);

        return tn_yes('获取角色权限规则信息成功',['data' => $data]);
    }

    /**
     * 添加角色权限规则
     * @http post
     * @url /auth_rule/add
     * @return \think\response\Json
     */
    public function addRule()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['pid','name','title','type','condition',
            'public_rule','sort','status']);

        $result = AuthRuleModel::addAuthRule($data);

        if ($result) {
            $this->request->log_content = '添加角色权限规则成功';
            return tn_yes('添加角色权限规则成功');
        }else {
            $this->request->log_content = '添加角色权限规则失败';
            return tn_no('添加角色权限规则失败');
        }
    }

    /**
     * 编辑角色权限规则
     * @http post
     * @url /auth_rule/edit
     * @return \think\response\Json
     */
    public function editRule()
    {
        $this->checkPutUrl();

        $data = $this->request->post(['id','pid','name','title','type','condition',
            'public_rule','sort','status']);

        $result = AuthRuleModel::editAuthRule($data);

        if ($result) {
            $this->request->log_content = '编辑角色权限规则成功';
            return tn_yes('编辑角色权限规则成功');
        }else {
            $this->request->log_content = '编辑角色权限规则失败';
            return tn_no('编辑角色权限规则失败');
        }
    }

    /**
     * 更新角色权限规则信息
     * @http put
     * @url /auth_rule/update
     * @return \think\response\Json
     */
    public function updateRule()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = AuthRuleModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新角色权限规则信息成功';
            return tn_yes('更新角色权限规则信息成功');
        }else {
            $this->request->log_content = '更新角色权限规则信息失败';
            return tn_no('更新角色权限规则信息失败');
        }
    }

    /**
     * 删除角色权限规则信息
     * @http delete
     * @url /auth_rule/delete
     * @return \think\response\Json
     */
    public function deleteRule()
    {
        $this->checkDeleteUrl();

        $ids = $this->request->delete('ids');

        $result = AuthRuleModel::delAuthRule($ids);

        if ($result) {
            $this->request->log_content = '删除角色权限规则信息成功';
            return tn_yes('删除角色权限规则信息成功');
        }else {
            $this->request->log_content = '删除角色权限规则信息失败';
            return tn_no('删除角色权限规则信息失败');
        }
    }
}