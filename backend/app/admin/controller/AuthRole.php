<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-15
 * Time: 09:29
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\admin\model\AuthRole as AuthRoleModel;

class AuthRole extends BaseController
{

    /**
     * 获取权限角色列表数据
     * @http GET
     * @url /auth_role/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        $data = AuthRoleModel::getPaginationList($params);

        return tn_yes('获取权限角色列表数据成功', return_vue_element_admin_pagination_data($data));
    }

    /**
     * 获取权限角色的名称数据
     * @http GET
     * @url /auth_role/all
     * @return \think\response\Json
     */
    public function getAll()
    {
        $token_data = $this->request->token_data;

        if ($token_data['uid'] == 1) {
            $data = AuthRoleModel::getDataWithField([['status','=',1]],['id','title','name'])
                ->toArray();
        } else {
            $data = AuthRoleModel::getDataWithField([['status','=',1],['id','<>', 1]],['id','title','name'])
                ->toArray();
        }

        return tn_yes('获取权限角色数据成功',['data' => $data]);
    }

    /**
     * 根据id获取指定的数据
     * @http GET
     * @url /auth_role/get_id
     * :id id值
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id');

        $data = AuthRoleModel::getByID($id);

//        $data = $role->getData();

        return tn_yes('获取权限角色信息成功',['data' => $data]);
    }

    /**
     * 添加权限角色
     * @http POST
     * @url /auth_role/add
     * @return \think\response\Json
     */
    public function addRole()
    {
        $this->checkPostUrl();

        // 获取post的数据
        $data = $this->request->post(['title','name','status'=>0,'menus','half_menus','rules']);

        $result = AuthRoleModel::addAuthRule($data);

        if ($result) {
            $this->request->log_content = '添加权限角色角色成功';
            return tn_yes('添加权限角色角色成功');
        }else {
            $this->request->log_content = '添加权限角色信息失败';
            return tn_no('添加权限角色信息失败');
        }
    }

    /**
     * 编辑权限角色
     * @http PUT
     * @url /auth_role/edit
     * @return \think\response\Json
     */
    public function editRole()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put(['id','title','name','status' => 0,'menus','half_menus','rules']);

        $result = AuthRoleModel::editAuthRule($data);

        if ($result) {
            $this->request->log_content = '编辑权限角色信息成功';
            return tn_yes('编辑权限角色信息成功');
        }else {
            $this->request->log_content = '编辑权限角色信息失败';
            return tn_no('编辑权限角色信息失败');
        }
    }

    /**
     * 更新权限角色信息
     * @http PUT
     * @url /auth_role/update
     * @return \think\response\Json
     */
    public function updateRole()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = AuthRoleModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新权限角色信息成功';
            return tn_yes('更新权限角色信息成功');
        }else {
            $this->request->log_content = '更新权限角色信息失败';
            return tn_no('更新权限角色信息失败');
        }
    }

    /**
     * 删除指定的权限信息
     * @http DELETE
     * @url /auth_role/delete
     * @return \think\response\Json
     */
    public function deleteRole()
    {
        $this->checkDeleteUrl();

        $data = $this->request->delete('ids');

        $result = AuthRoleModel::delAuthRole($data);

        if ($result) {
            $this->request->log_content = '删除权限角色信息成功';
            return tn_yes('删除权限角色信息成功');
        }else {
            $this->request->log_content = '删除权限角色信息失败';
            return tn_no('删除权限角色信息失败');
        }
    }
}